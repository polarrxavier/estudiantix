# -*- coding: utf-8 -*-
{
    'name': "StudentsRecords",
    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",
    'description': """
        Long description of module's purpose
    """,
    'author': "Jexier",
    'website': "http://www.yourcompany.com",
    'category': 'Uncategorized',
    'version': '0.2',
    'depends': ['base'],

    'data': [
        'security/ir.model.access.csv',
        'views/menu.xml',
        'views/students_views.xml',
        'views/res_partner_views.xml',
        'views/subjects_views.xml',
        'views/students_notes_views.xml',
        'views/search_views.xml'
    ],
}
