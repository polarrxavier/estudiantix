from odoo import models, fields, api
from odoo.exceptions import ValidationError


class Subjects(models.Model):
    _name = 'students.subjects'
    name = fields.Char(string="Name of the subject")

class Notes(models.Model):
    _name = 'students.notes'

    # Validamos que el campo 'note' no exeda los limites de notas permitidos
    @api.multi
    @api.constrains('note')
    def _val_note(self):
        for record in self: #Se valida por cada registro a mostrar
            if record.note < 1 or record.note > 10:
                raise ValidationError('La nota debe estar entre 1 y 10')

    # Aquí se relaciona al modelo 'students.subjects'
    subject_id = fields.Many2one('students.subjects', string='Subject', ondelete='cascade')
    # Aquí se relaciona al modelo 'students.students'
    student_id = fields.Many2one('students.students', string='Student') 
    note = fields.Integer(string="Note", default=1)
