from odoo import models, fields, api


class ResUsers(models.Model):
    _inherit = 'res.users'

    is_student = fields.Boolean(string='¿Es estudiante?')
