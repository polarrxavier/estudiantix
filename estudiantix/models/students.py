from odoo import models, fields, api
from odoo.exceptions import ValidationError


class Students(models.Model):
    _name = 'students.students'

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        if self.partner_id:
            if self.partner_id.id == 1: # Aquí hacemos que el self.partner_id sea igual al registro 1
                self.partner_id = False
                # Aquí le retornamos un dicionario con un dic warning que
                # contiene un diccionario con una clave "message" y un
                # valor como mensaje
                return {
                    'source': [],
                    'warning': {
                        'message': 'Not select the Company profile!.'
                    }
                }

            # def c(**kw):
            #     for key, value in kw.items():
            #         print(key, value)
            # c(saludo='Hola', p2='Como')
            # d = {'saludo': 'Hola2', 'p2': 'ASDASD', 'B': {'a': 'b'}}
            # c(**d)
                
    # @api.onchange('name_more_average')
    # def _onchange_name_more_average(self):
    #     if self.name_more_average:
    #         domain = [('id', '=', self.name_more_average)]
    #         return {
    #             'domain': {
    #                 'partner_id': domain,
    #             }
    #         }

    # Con api.multi se recorren todos los registros que trae self, y 
    # luego se iteran con un for
    @api.multi
    def _compute_average(self):
        for record in self:
            if record.note_ids: # Si hay un registro con el objeto note_ids tambien lo itera
                record.average = sum(record.note_ids.mapped('note')) / len(record.note_ids)

    partner_id = fields.Many2one('res.partner', string='Profile')
    image = fields.Binary(related="partner_id.image", readonly=True)
    identification = fields.Integer(related='partner_id.identification')
    name = fields.Char(related='partner_id.name')
    lastname = fields.Char(related='partner_id.lastname')
    email = fields.Char(related='partner_id.email')
    grade = fields.Integer(string='Grade', required=True, default=0)
    average = fields.Integer(string='Average', compute=_compute_average, default=0)
    note_ids = fields.One2many(comodel_name='students.notes', inverse_name='student_id', string='Notes')
    # El campo inverso siempre va al contrario del modelo al que fue llamado
    #name_more_average = fields.Char(string='Nombre+Promedio')