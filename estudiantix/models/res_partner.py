from email.policy import default
from odoo import models, fields, api
from odoo.exceptions import ValidationError

class ResPartner(models.Model):
    _inherit = 'res.partner'
    # @api.onchange('is_student')
    # def _onchange_is_student(self):

    # Verificamos que exista en la base de dalos el identification que estamos ingresando
    # "_sql_constraints" Restricciones de SQL [(nombre, sql_definicion, mensaje)]
    _sql_constraints = [("val-id", "unique(identification)", "Identificador ya existe en la base de datos")]
    identification = fields.Integer(String='Identification', required=True)
    lastname = fields.Char(string='Lastname', required=True)
    # is_student = fields.Boolean(string='Is student?')
    
